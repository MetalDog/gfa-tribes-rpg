const express = require('express');
const userController = require('../controllers/addUserController');
const helloWorldController = require('../controllers/hello-world');

const router = express.Router();

router.get('/helloworld', helloWorldController.helloWorldController);

router.post('/register', userController.registerNewUser);

module.exports = router;
