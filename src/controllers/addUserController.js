const userService = require('../services/user/addUser');
const kingdomService = require('../services/kingdom/createKingdom');
const userAndPasswordChecker = require('../services/user/checkIfUserAndPassExist');
const userChecker = require('../services/user/checkIfUserExists');
const passwordChecker = require('../services/user/checkIfPassExists');
const passwordLengthChecker = require('../services/user/checkPasswordLength');

const registerNewUser = (req, res) => {
  const { username, password, kingdomName } = req.body;

  userAndPasswordChecker.checkIfUserAndPasswordExist(username, password)
    .then(() => userChecker.checkIfUsernameProvided(username))
    .then(() => passwordChecker.checkIfPasswordProvided(password))
    .then(() => passwordLengthChecker.checkPasswordLength(password))
    .then(() => userService.addUser(username, password))
    .then(userId => kingdomService.createKingdom(userId.id, username, kingdomName))
    .then((data) => {
      res.json({
        userId: data.userId, username, kingdomId: data.id,
      });
    })
    .catch((err) => {
      if (err.errors) {
        res.status(409).json(err.errors.username.message);
      } else {
        res.status(400).json(err.message);
      }
    });
};

module.exports = { registerNewUser };
