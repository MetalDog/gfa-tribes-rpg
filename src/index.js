const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
const PORT = 3000;
const router = require('./routes/index');
require('dotenv').config();

app.use(express.json());
app.use(cors());
app.use(router);

mongoose.connect(`mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0-e9s5o.mongodb.net/Tribes?retryWrites=true`,
  {
    useNewUrlParser: true,
    useCreateIndex: true,
  })
  // eslint-disable-next-line no-console
  .then(() => console.log('MongoDB successfully connected'))
  // eslint-disable-next-line no-console
  .catch(err => console.error(err));

app.listen(PORT, () => {
  console.log(`Port is listening on ${PORT}`); // eslint-disable-line
});

module.exports = app;
