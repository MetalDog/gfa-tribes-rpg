const checkPasswordLength = password => new Promise((resolve, reject) => {
  if (password.length < 8) {
    reject(new Error('Your password must have at least 8 chars'));
  } else {
    resolve();
  }
});

module.exports = {
  checkPasswordLength,
};
