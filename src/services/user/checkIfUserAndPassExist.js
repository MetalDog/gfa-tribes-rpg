const checkIfUserAndPasswordExist = (username, password) => new Promise((resolve, reject) => {
  if (username.length === 0 && password.length === 0) {
    reject(new Error('Please provide username and password.'));
  } else {
    resolve();
  }
});

module.exports = { checkIfUserAndPasswordExist };
