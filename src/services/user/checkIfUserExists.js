const checkIfUsernameProvided = username => new Promise((resolve, reject) => {
  if (!username) {
    reject(new Error('Please provide a username'));
  } else {
    resolve();
  }
});

module.exports = {
  checkIfUsernameProvided,
};
