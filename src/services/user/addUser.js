const User = require('../../models/User.model');

const addUser = (username, password) => new Promise((resolve, reject) => {
  const newUser = new User({ username, password });

  newUser.save((err, data) => {
    if (err) {
      reject(err);
    } else {
      resolve(data);
    }
  });
});

module.exports = { addUser };
