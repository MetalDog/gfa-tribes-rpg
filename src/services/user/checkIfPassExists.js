const checkIfPasswordProvided = password => new Promise((resolve, reject) => {
  if (!password) {
    reject(new Error('Please provide a password'));
  } else {
    resolve();
  }
});

module.exports = {
  checkIfPasswordProvided,
};
