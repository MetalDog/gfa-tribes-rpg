const Kingdom = require('../../models/Kingdom.model');

const createKingdom = (userId, username, kingdomName) => new Promise((resolve, reject) => {
  let realKingdomName;
  if (kingdomName.length < 1) {
    realKingdomName = `${username}'s kingdom`;
  }

  const newKingdom = new Kingdom({ realKingdomName, userId });
  newKingdom.save((err, data) => {
    if (err) {
      reject(err);
    } else {
      resolve(data);
    }
  });
});

module.exports = { createKingdom };
