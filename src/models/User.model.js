const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const User = mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
  },

  password: {
    type: String,
    required: true,

  },
});

User.plugin(uniqueValidator, { message: 'Username is already taken!' });

module.exports = mongoose.model('User', User);
