const mongoose = require('mongoose');

const Kingdom = mongoose.Schema({
  kingdomName: {
    type: String,
    required: true,
  },

  userId: {
    type: String,
    required: true,
  },
});


module.exports = mongoose.model('Kingdom', Kingdom);
